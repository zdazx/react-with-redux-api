package com.example.reactwithreduxAPI;

public class UserProfile {
    private String name;
    private String gender;
    private String description;

    public UserProfile(String name, String gender, String description) {
        this.name = name;
        this.gender = gender;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getDescription() {
        return description;
    }
}
